/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use super::Storage;
use anyhow::Result;
use log::warn;
use std::path::Path;

#[derive(Debug)]
pub(crate) struct Client {}

impl Storage for Client {
    fn upload(&self, _p: &Path) -> Result<String> {
        warn!("dummy upload: doing nothing");
        Ok(String::from("n/a"))
    }
}
