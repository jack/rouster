/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use super::Storage;
use anyhow::Result;
use futures::executor::block_on;
use log::{debug, info};
use reqwest::{
    header::{HeaderMap, HeaderValue, AUTHORIZATION},
    Body,
};
use serde::{Deserialize, Serialize};
use std::path::Path;
use tokio::fs::File;
use tokio_util::codec::{BytesCodec, FramedRead};

#[derive(Serialize, Debug)]
#[serde(rename_all = "camelCase")]
struct UploadUrlRequest {
    bucket_id: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
struct UploadUrlResponse {
    bucket_id: String,
    upload_url: String,
    authorization_token: String,
}

#[derive(Debug, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
struct UploadResponse {
    account_id: String,
    action: String,
    bucket_id: String,
    content_length: u64,
    content_sha1: String,
    content_type: String,
    file_id: String,
    file_name: String,
    upload_timestamp: u64,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
struct AuthorizeResponse {
    account_id: String,
    authorization_token: String,
    api_url: String,
    download_url: String,
}

#[derive(Debug)]
pub(crate) struct Client {
    key_id: String,
    key: String,
    bucket_id: String,
}

impl Client {
    pub(crate) fn new(key_id: &str, key: &str, bucket_id: &str) -> Self {
        Self {
            key_id: key_id.trim().to_string(),
            key: key.trim().to_string(),
            bucket_id: bucket_id.trim().to_string(),
        }
    }

    fn authorize(&self) -> Result<AuthorizeResponse> {
        // Build a client
        let client = reqwest::Client::new();
        let res = block_on(
            client
                .get("https://api.backblazeb2.com/b2api/v2/b2_authorize_account")
                .basic_auth(&self.key_id, Some(&self.key))
                .send(),
        )?;

        // Check for an error response
        res.error_for_status_ref()?;

        let json = block_on(res.json())?;

        Ok(json)
    }
}

impl Storage for Client {
    fn upload(&self, p: &Path) -> Result<String> {
        info!("uploading image to backblaze b2");
        let auth = self.authorize()?;

        // Set up the headers
        let mut headers = HeaderMap::new();
        headers.insert(
            AUTHORIZATION,
            HeaderValue::from_str(&auth.authorization_token)?,
        );
        debug!("upload url headers: {:?}", &headers);

        // Build a client
        let client = reqwest::Client::new();
        let req = client
            .post(&format!(
                "{}/{}",
                &auth.api_url, "b2api/v2/b2_get_upload_url"
            ))
            .headers(headers)
            .json(&UploadUrlRequest {
                bucket_id: self.bucket_id.clone(),
            });
        debug!("{:?}", &req);
        let res = block_on(req.send())?;
        res.error_for_status_ref()?;

        let res: UploadUrlResponse = block_on(res.json())?;
        debug!("{:?}", res);

        // Get the name of the file we're going to send
        let name = p.file_name().unwrap().to_str().unwrap();
        // Open the file we're going to send
        let file = block_on(File::open(p))?;

        // Set up the headers for the actual upload
        let mut headers = HeaderMap::new();
        headers.insert(
            AUTHORIZATION,
            HeaderValue::from_str(&res.authorization_token)?,
        );
        headers.insert("x-bz-file-name", HeaderValue::from_str(name)?);
        headers.insert("content-type", HeaderValue::from_static("image/png"));
        let len = format!("{}", block_on(file.metadata())?.len());
        headers.insert("content-length", HeaderValue::from_str(&len)?);
        // TODO: Calculate the sha checksum
        headers.insert(
            "x-bz-content-sha1",
            HeaderValue::from_static("do_not_verify"),
        );
        debug!("upload headers: {:?}", &headers);

        let res = block_on(
            client
                .post(&res.upload_url)
                .headers(headers)
                .body(Body::wrap_stream(FramedRead::new(file, BytesCodec::new())))
                .send(),
        )?;
        // Check for an error response
        res.error_for_status_ref()?;
        let res: UploadResponse = block_on(res.json())?;

        debug!("{:?}", res);

        let url = format!("https://f001.backblazeb2.com/file/rouster/{}", &name);
        Ok(url)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::offset::Utc;

    #[test]
    // This is a test to ensure that the type we're using for the
    // upload_timestamp field of the UploadResponse is large enough on all
    // platforms to handle the integer.
    //
    // TODO: Actually test the deserialization of a JSON string with the
    // timestamp in it, since this test could get out of date.
    fn test_upload_response_upload_timestamp() {
        assert!(i64::MAX > Utc::now().timestamp_millis());
    }

    #[test]
    fn check_timestamp() {
        let _res = UploadResponse {
            upload_timestamp: 1597896224000,
            ..Default::default()
        };
    }
}
