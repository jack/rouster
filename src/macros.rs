/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#[macro_export]
macro_rules! display_image {
    ($canvas:expr, $name:expr) => {
        #[cfg(feature = "feh")]
        {
            use std::{fs, process::Command};

            let path = format!("/tmp/{}.png", $name);
            $canvas.write(&path).unwrap();

            Command::new("feh")
                .args(&[&path, "-d", "--info", "file %f", "--draw-tinted"])
                .output()
                .ok();

            fs::remove_file(path).unwrap();
        }
    };
}
