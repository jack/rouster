/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::Result;
use log::info;
use std::path::{Path, PathBuf};

#[derive(Debug)]
pub(crate) struct RealFramebuffer {
    pub(crate) buffer: framebuffer::Framebuffer,
    pub(crate) width: u32,
    pub(crate) height: u32,
    pub(crate) line_length: u32,
    pub(crate) bytes_per_pixel: u32,
}

#[derive(Debug)]
pub(crate) struct FakeFramebuffer {
    pub(crate) path: PathBuf,
    pub(crate) width: u32,
    pub(crate) height: u32,
    pub(crate) line_length: u32,
    pub(crate) bytes_per_pixel: u32,
}

pub(crate) trait Framebuffer: std::fmt::Debug {
    fn width(&self) -> f64;
    fn height(&self) -> f64;
    fn line_length(&self) -> u32;
    fn bytes_per_pixel(&self) -> u32;
    fn write(&mut self, buffer: &[u8]);
}

impl RealFramebuffer {
    pub(crate) fn new<T: AsRef<Path>>(device_path: T) -> Result<Self> {
        info!("displaying output on {:?}", device_path.as_ref());
        let framebuffer = framebuffer::Framebuffer::new(device_path)?;

        Ok(Self {
            width: framebuffer.var_screen_info.xres,
            height: framebuffer.var_screen_info.yres,
            line_length: framebuffer.fix_screen_info.line_length,
            bytes_per_pixel: framebuffer.var_screen_info.bits_per_pixel / 8,
            buffer: framebuffer,
        })
    }
}

impl Framebuffer for RealFramebuffer {
    fn width(&self) -> f64 {
        self.width as f64
    }

    fn height(&self) -> f64 {
        self.height as f64
    }

    fn line_length(&self) -> u32 {
        self.line_length
    }

    fn bytes_per_pixel(&self) -> u32 {
        self.bytes_per_pixel
    }

    fn write(&mut self, buf: &[u8]) {
        self.buffer.write_frame(buf);
    }
}

impl FakeFramebuffer {
    pub(crate) fn new<T: AsRef<Path>>(path: T, width: u32, height: u32) -> Self {
        info!("displaying output at {:?}", path.as_ref());

        Self {
            path: PathBuf::from(path.as_ref()),
            width,
            height,
            line_length: width * 2,
            bytes_per_pixel: 2,
        }
    }
}

impl Framebuffer for FakeFramebuffer {
    fn width(&self) -> f64 {
        self.width as f64
    }

    fn height(&self) -> f64 {
        self.height as f64
    }

    fn line_length(&self) -> u32 {
        self.line_length
    }

    fn bytes_per_pixel(&self) -> u32 {
        self.bytes_per_pixel
    }

    fn write(&mut self, _buf: &[u8]) {
        // TODO: Somehow write this to an image file or something.
    }
}
