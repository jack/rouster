/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use super::Notification;
use anyhow::Result;
use futures::executor::block_on;
use log::{debug, error, info};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Debug)]
#[serde(rename_all = "PascalCase")]
struct SendRequest<'a> {
    body: &'a str,
    from: &'a str,
    media_url: Option<&'a str>,
    to: &'a str,
}

#[derive(Debug, Deserialize)]
struct SendResponse {
    body: String,
    error_code: Option<String>,
    error_message: Option<String>,
    status: String,
    to: String,
}

#[derive(Debug)]
pub(crate) struct Client {
    account_sid: String,
    from: String,
    auth_token: String,
    to: Vec<String>,
}

impl Client {
    pub(crate) fn new(account_sid: &str, auth_token: &str, from: &str) -> Self {
        Self {
            account_sid: account_sid.trim().to_string(),
            from: from.trim().to_string(),
            auth_token: auth_token.trim().to_string(),
            to: vec![],
        }
    }
}

impl Notification for Client {
    fn send(&self, to: &str, media_url: Option<&str>) -> Result<()> {
        /*
        curl -X POST https://api.twilio.com/2010-04-01/Accounts/ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/Messages.json \
        --data-urlencode "Body=Hello there$EXCLAMATION_MARK" \
        --data-urlencode "From=+15555555555" \
        --data-urlencode "MediaUrl=https://demo.twilio.com/owl.png" \
        --data-urlencode "To=+12316851234" \
        -u ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:your_auth_token
            */
        info!("sending image to {} via twilio", to);

        let message = if let Some(url) = media_url {
            format!("roast complete: {}", url)
        } else {
            "roast complete".to_string()
        };

        // Build a client
        let client = reqwest::Client::new();
        let req = client
            .post(&format!(
                "https://api.umatilla.us1.twilio.com/2010-04-01/Accounts/{}/Messages.json",
                &self.account_sid,
            ))
            .basic_auth(&self.account_sid, Some(&self.auth_token))
            .form(&SendRequest {
                body: &message,
                from: &self.from,
                media_url: media_url,
                to: to,
            });
        debug!("{:#?}", &req);
        let res = block_on(req.send())?;
        if let Err(e) = res.error_for_status_ref() {
            error!("{:?}", block_on(res.text())?);

            panic!(e);
        }

        let res: SendResponse = block_on(res.json())?;
        debug!("{:#?}", res);

        if let Some(code) = res.error_code {
            let msg = res.error_message.unwrap_or("unknown".to_string());
            error!("twilio error sending notification: {}: {}", code, msg);
        }

        info!("done sending image to {} via twilio", to);

        Ok(())
    }
}
