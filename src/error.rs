/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use thiserror::Error;

#[derive(Error, Debug)]
pub(crate) enum MagickError {
    #[error("MagickError::WriteImage: error writing image: {0}")]
    WriteImage(&'static str),

    #[error("MagickError::AnnotateImage: error annotating image: {0}")]
    AnnotateImage(&'static str),

    #[error("MagickError::Font: error setting font: {0}")]
    Font(&'static str),

    #[error("MagickError::NewImage: error creating new image on wand: {0}")]
    NewImage(&'static str),

    #[error("MagickError::SetColor: error setting color on pixel wand: {0}")]
    SetColor(&'static str),
}
