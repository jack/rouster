/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use super::events::Event;
use crossbeam::channel;
use log::debug;
use std::thread;
use sysfs_gpio::{Direction, Edge, Pin};

const HIGH: u8 = 255;
const LOW: u8 = 0;

pub(crate) fn interrupt(
    pin: u64,
    high_event: Event,
    low_event: Event,
    chan: channel::Sender<Event>,
) {
    let input = Pin::new(pin);

    thread::spawn(move || loop {
        input
            .with_exported(|| {
                input.set_direction(Direction::In)?;
                input.set_edge(Edge::BothEdges)?;
                let mut poller = input.get_poller()?;
                loop {
                    if let Some(value) = poller.poll(1000)? {
                        debug!("interrupt on pin {}: {}", pin, value);
                        match value {
                            HIGH => chan.send(high_event.clone()).unwrap(),
                            LOW => chan.send(low_event.clone()).unwrap(),
                            _ => debug!("value doesn't match HIGH or LOW: {}", value),
                        }
                    }
                }
            })
            .unwrap();
    });
}
