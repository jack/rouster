use crate::Event;
use crossbeam::channel;
use pprof::protos::Message as _;
use std::{thread::sleep, time::Duration};
use warp::Filter;

use crate::REGISTRY;

// Serve prometheus on port 8080.
pub(crate) async fn serve_metrics(tx: channel::Sender<Event>) {
    let metrics_route = warp::path!("rouster" / "metrics").and_then(metrics_handler);
    let pprof_route = warp::path!("rouster" / "pprof").and_then(pprof_handler);

    let tx_reset = tx.clone();

    println!("starting to serve prometheus routes on port 8080");
    warp::serve(
        metrics_route
            .or(pprof_route)
            .or(warp::path!("rouster" / "reset").map(move || {
                tx_reset.send(Event::Reset).unwrap();

                "success"
            }))
            .or(warp::path!("rouster" / "runhold").map(move || {
                tx.send(Event::RunHold).unwrap();

                "success"
            })),
    )
    .run(([0, 0, 0, 0], 8080))
    .await;
}

async fn pprof_handler() -> Result<impl warp::Reply, warp::Rejection> {
    let guard = pprof::ProfilerGuard::new(100).unwrap();
    sleep(Duration::from_secs(5));
    let profile = guard.report().build().unwrap().pprof().unwrap();
    let mut content = vec![];
    profile.encode(&mut content).unwrap();

    Ok(content)
}

async fn metrics_handler() -> Result<impl warp::Reply, warp::Rejection> {
    // This is in the function because the Encoder trait collides with the pprof
    // Message trait.
    use prometheus::{Encoder as _, TextEncoder};

    let mut buffer = vec![];
    TextEncoder::new()
        .encode(&REGISTRY.gather(), &mut buffer)
        .unwrap();

    Ok(String::from_utf8(buffer).unwrap())
}
