/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use crate::error::MagickError;
use crate::plot;
use anyhow::Result;
use log::{debug, trace};
use magick_rust::{
    bindings::MagickBooleanType_MagickFalse as MagickFalse, magick_wand_genesis, DrawingWand,
    MagickWand, PixelWand,
};
use std::{convert::TryInto, path::Path, sync::Once};

// Used to make sure MagickWand is initialized exactly once. Note that we
// do not bother shutting down, we simply exit when we're done.
static START: Once = Once::new();

pub(crate) struct Canvas {
    wand: MagickWand,
    width: u64,
    height: u64,
}

pub enum Alignment {
    Top,
    TopRight,
    Right,
    BottomRight,
    Bottom,
    BottomLeft,
    Left,
    TopLeft,
    Center,
}

impl From<&Alignment> for u32 {
    fn from(align: &Alignment) -> u32 {
        match align {
            Alignment::Top => magick_rust::bindings::GravityType_NorthGravity,
            Alignment::TopRight => magick_rust::bindings::GravityType_NorthEastGravity,
            Alignment::Right => magick_rust::bindings::GravityType_EastGravity,
            Alignment::BottomRight => magick_rust::bindings::GravityType_SouthEastGravity,
            Alignment::Bottom => magick_rust::bindings::GravityType_SouthGravity,
            Alignment::BottomLeft => magick_rust::bindings::GravityType_SouthWestGravity,
            Alignment::Left => magick_rust::bindings::GravityType_WestGravity,
            Alignment::TopLeft => magick_rust::bindings::GravityType_NorthWestGravity,
            Alignment::Center => magick_rust::bindings::GravityType_CenterGravity,
        }
    }
}

impl Canvas {
    /// Create a new Canvas to use for building images on top of. You can pass
    /// color names for the bgcolor argument, or None to get a transparent
    /// background.
    pub(crate) fn new(width: u64, height: u64, bgcolor: Option<&str>) -> Result<Self> {
        // Initialize magickwand, but only once.
        START.call_once(|| {
            magick_wand_genesis();
        });

        // Set up a new wand
        let wand = MagickWand::new();

        // Make a pixel wand to set the background
        let mut pixel = PixelWand::new();
        match bgcolor {
            Some(color) => pixel.set_color(color).map_err(MagickError::SetColor)?,
            None => pixel.set_color(&"none").map_err(MagickError::SetColor)?,
        }

        // Create a new image on the wand.
        wand.new_image(width.try_into()?, height.try_into()?, &pixel)
            .map_err(MagickError::NewImage)?;

        Ok(Self {
            wand,
            width,
            height,
        })
    }

    /// `font_height`, `offset_x`and `offset_y` are percentages of the total screen height.
    pub(crate) fn annotate<T: AsRef<str>>(
        &mut self,
        text: T,
        font_height: f64,
        offset_x: f64,
        offset_y: f64,
        alignment: &Alignment,
        color: &str,
    ) -> Result<&Self> {
        // Figure out the font height
        let font_size = (self.height as f64 * font_height / 100.0).round();

        // Figure out the offsets
        let offset_x = offset_x / 100.0 * self.width as f64;
        let offset_y = offset_y / 100.0 * self.height as f64;

        // Make a pixel wand to set the color.
        let mut pixel = PixelWand::new();
        pixel.set_color(color.as_ref()).unwrap();

        // Make a drawing wand to add the text.
        let mut draw = DrawingWand::new();
        draw.set_text_antialias(MagickFalse);
        draw.set_font_size(font_size);
        draw.set_fill_color(&pixel);
        draw.set_font("DejaVu-Sans-Mono")
            .map_err(MagickError::Font)?;
        draw.set_gravity(u32::from(alignment));
        trace!(
            "coords: ({},{}), gravity: {:?}",
            offset_x,
            offset_y,
            draw.get_gravity()
        );

        // Add the text to the image. We ignore rotation for now.
        self.wand
            .annotate_image(&draw, offset_x, offset_y, 0.0, text.as_ref())
            .map_err(MagickError::AnnotateImage)?;

        Ok(self)
    }

    /// Write the image to disk.
    pub(crate) fn write<T: AsRef<Path>>(&self, path: T) -> Result<()> {
        Ok(self
            .wand
            .write_image(path.as_ref().to_str().unwrap())
            .map_err(MagickError::WriteImage)?)
    }

    pub(crate) fn plot_series(&mut self, temp: &[(u64, f32)], ror: &[(u64, f32)]) -> Result<()> {
        // Plot the series and return the path to a tempfile which has the
        // image. We use some percentage of the height of the main image for the
        // chart; we'll the offset it down when we compose it below.
        let path = plot::plot_series(
            self.width as u32,
            (0.73 * self.height as f64) as u32,
            temp,
            ror,
        )
        .unwrap();
        debug!("canvas: done plotting series");

        let wand = MagickWand::new();
        wand.read_image(path.0.to_str().unwrap()).unwrap();

        // Shift the image down by a percentage to avoid the text at the top.
        // It's less than the reciprocal, because we have text at the bottom we
        // need to avoid.
        self.wand
            .compose_images(
                &wand,
                magick_rust::bindings::CompositeOperator_SrcOverCompositeOp,
                false,
                0,
                ((0.22 * self.height as f64) as i64).try_into()?,
            )
            .unwrap();

        Ok(())
    }

    pub(crate) fn frame(&self) -> Result<Vec<u8>> {
        // Set up a vector of u8 points representing a line of pixels
        let mut frame = vec![0_u8; (self.width * self.height * 2).try_into()?];
        debug!("done making frame vec");

        let pixels = self
            .wand
            .export_image_pixels(0, 0, self.width.try_into()?, self.height.try_into()?, "RGB")
            .unwrap();

        let mut i = 0;
        let mut n = 0;
        while i < pixels.len() {
            let red = pixels[i];
            let green = pixels[i + 1];
            let blue = pixels[i + 2];

            // Get the high and low bytes for this pixel, since it's a 16-bit
            // framebuffer, which means it uses 2 8bit bytes to set the color.
            // See https://en.wikipedia.org/wiki/High_color for details.
            let (high, low) = make_bytes(blue, red, green)?;

            // Set the bytes in the frame.
            frame[n] = high;
            frame[n + 1] = low;

            i += 3;
            n += 2;
        }

        Ok(frame)
    }
}

fn make_bytes(r: u8, g: u8, b: u8) -> Result<(u8, u8)> {
    trace!("r before shift: {:>016b}", r);
    let r: u16 = ((r as u16) >> 3) << 11;
    trace!("r after shift:  {:>016b}", r);

    trace!("g before shift: {:>016b}", g);
    let g: u16 = ((g as u16) >> 2) << 5;
    trace!("g after shift:  {:>016b}", g);

    trace!("b before shift: {:>016b}", b);
    let b: u16 = (b as u16) >> 3;
    trace!("b after shift:  {:>016b}", b);

    let composite = r | g | b;
    trace!("composite:      {:>016b}", composite);

    let bytes = composite.to_be_bytes();
    trace!("bytes: {:>08b} {:>08b}", bytes[0], bytes[1]);

    Ok((bytes[0], bytes[1]))
}

#[cfg(test)]
mod tests {
    extern crate test;

    use super::*;
    use rand::{thread_rng, Rng};
    use test::{black_box, Bencher};

    #[test]
    fn test_frame() {
        let canvas = Canvas::new(320, 240, Some("white")).unwrap();

        // This is the line length and bytespp from the pi
        let frame = canvas.frame().unwrap();

        // This is the length of the frame as read off the buffer on the pi
        assert_eq!(frame.len(), 153600);
    }

    #[bench]
    fn bench_frame(b: &mut Bencher) {
        let canvas = Canvas::new(320, 240, Some("white")).unwrap();

        b.iter(|| canvas.frame());
    }

    #[bench]
    fn bench_get_image_pixel_color(b: &mut Bencher) {
        let canvas = Canvas::new(320, 240, Some("white")).unwrap();
        // Make sure it works
        canvas
            .wand
            .get_image_pixel_color(black_box(23), black_box(34))
            .unwrap();

        b.iter(|| {
            canvas
                .wand
                .get_image_pixel_color(black_box(23), black_box(34))
        });
    }

    #[test]
    fn test_make_bytes() {
        let (h, l) = make_bytes(40, 40, 40).unwrap();
        assert_eq!(h, 0b00101001u8);
        assert_eq!(l, 0b01000101u8);

        let (h, l) = make_bytes(255, 0, 255).unwrap();
        assert_eq!(h, 0b11111000u8);
        assert_eq!(l, 0b00011111u8);
    }

    #[bench]
    fn bench_make_bytes(b: &mut Bencher) {
        // Make sure that our function call is valid.
        make_bytes(40, 40, 40).unwrap();

        // Now actually bench it.
        //b.iter(|| make_bytes((black_box(15.6 * 255.0) as u8), black_box(40), black_box(40)));
        b.iter(|| {
            make_bytes(
                black_box((15.6 * 255.0) as u8),
                black_box((15.6 * 255.0) as u8),
                black_box((15.6 * 255.0) as u8),
            )
        });
    }

    #[test]
    fn test_new() {
        Canvas::new(100, 100, None).unwrap();
        Canvas::new(10, 10, Some(&"blue")).unwrap();
    }

    #[bench]
    fn bench_new(b: &mut Bencher) {
        b.iter(|| Canvas::new(black_box(320), black_box(240), black_box(Some("white"))).unwrap());
    }

    #[test]
    fn test_plot() {
        let mut canvas = Canvas::new(320, 240, Some("white")).unwrap();

        let readings = readings();

        canvas.plot_series(&readings, &readings).unwrap();
    }

    fn readings() -> Vec<(u64, f32)> {
        let mut rng = thread_rng();
        let mut readings = vec![];
        for x in 0..600 {
            let diff = rng.gen_range(-3.0, 3.0);
            readings.push((x, readings.last().unwrap_or(&(0, 0.0)).1 + diff));
        }

        readings
    }

    #[bench]
    fn bench_plot(b: &mut Bencher) {
        let mut canvas = Canvas::new(320, 240, Some("white")).unwrap();
        let readings = readings();

        b.iter(|| {
            canvas
                .plot_series(black_box(&readings), black_box(&readings))
                .unwrap()
        });
    }

    #[test]
    fn test_annotate() {
        let fonts: [f64; 2] = [10.0, 5.0];

        for &font in fonts.iter() {
            let mut canvas = Canvas::new(320, 240, Some("white")).unwrap();
            canvas
                .annotate(&"top left", font, 0.0, 0.0, &Alignment::TopLeft, &"gray")
                .unwrap();
            canvas
                .annotate(
                    &"bottom left",
                    font,
                    0.0,
                    0.0,
                    &Alignment::BottomLeft,
                    &"red",
                )
                .unwrap();
            canvas
                .annotate(
                    &"bottom right",
                    font,
                    0.0,
                    0.0,
                    &Alignment::BottomRight,
                    &"blue",
                )
                .unwrap();
            canvas
                .annotate(&"top right", font, 0.0, 0.0, &Alignment::TopRight, &"green")
                .unwrap();
        }
    }
}
