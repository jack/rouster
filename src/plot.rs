/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::Result;
use chrono::offset::Utc;
use log::debug;
use plotters::prelude::*;
use std::{fs, ops::Range, path::PathBuf};

const GRAY: RGBColor = RGBColor(128, 128, 128);

pub(crate) struct PlotPath(pub(crate) PathBuf);

/// This cleans up the tmp files created by the plotting library, once they're
/// no longer in used by the caller.
impl Drop for PlotPath {
    fn drop(&mut self) {
        fs::remove_file(&self.0).unwrap();
    }
}

fn domain_and_range(series: &[(u64, f32)]) -> (Range<u64>, Range<f32>) {
    if series.is_empty() {
        return (0..1, -1.0..1.0);
    }

    let (x_min, x_max, y_min, y_max) = series.iter().fold(
        (u64::MAX, u64::MIN, f32::MAX, f32::MIN),
        |(mut x_min, mut x_max, mut y_min, mut y_max), (x, y)| {
            if *x < x_min {
                x_min = *x;
            }

            if *x > x_max {
                x_max = *x;
            }

            if *y < y_min {
                y_min = *y;
            }

            if *y > y_max {
                y_max = *y;
            }

            (x_min, x_max, y_min, y_max)
        },
    );

    (x_min..x_max + 1, y_min..y_max + 1.0)
}

pub(crate) fn plot_series(
    width: u32,
    height: u32,
    temp: &[(u64, f32)],
    ror: &[(u64, f32)],
) -> Result<PlotPath> {
    // Create a path where we'll store the image.
    // TODO: Figure out how to keep this all in memory.
    let path = PathBuf::from(format!("/tmp/{}.png", Utc::now().timestamp_nanos()));

    // Set up our backend.
    let root = BitMapBackend::new(&path, (width, height)).into_drawing_area();

    // Set the background color.
    // TODO: Make this transparent, so we can overlay it in the main image?
    root.fill(&WHITE)?;

    // Set up a chart builder in the root.
    let (temp_domain, mut temp_range) = domain_and_range(temp);
    if temp_range.end < 224.0 {
        temp_range.end = 224.0;
    }
    let (ror_domain, mut ror_range) = domain_and_range(ror);
    if (ror_range.start - ror_range.end).abs() == 0.0 {
        ror_range.start -= 1.0;
        ror_range.end += 1.0;
    }
    let mut chart = ChartBuilder::on(&root)
        .margin(3)
        .x_label_area_size(15)
        .y_label_area_size(30)
        .right_y_label_area_size(30)
        .build_ranged(temp_domain, temp_range)?
        .set_secondary_coord(ror_domain, ror_range);
    debug!("configured chartbuilder");

    // Set the mesh background in the builder.
    chart
        .configure_mesh()
        .disable_mesh()
        .x_label_formatter(&|l| format!("{}:{:02}", l / 60, l % 60))
        .x_labels(12)
        .y_label_formatter(&|l| format!("{:.0}", l))
        .y_desc("temp")
        .draw()?;
    debug!("configured mesh");

    if !ror.is_empty() {
        chart
            .configure_secondary_axes()
            .y_desc("ror")
            .y_label_formatter(&|l| format!("{:.0}", l))
            .draw()?;
        debug!("configured secondary axes");

        // Plot the RoR.
        chart
            .draw_secondary_series(LineSeries::new(ror.iter().map(|(x, y)| (*x, *y)), &BLUE))?
            .label("RoR")
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 10, y)], &BLUE));
        debug!("plotted ror");
    }

    // Plot the temperature.
    chart
        .draw_series(LineSeries::new(temp.iter().map(|(x, y)| (*x, *y)), &RED))?
        .label("temp")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 10, y)], &RED));
    debug!("plotted temp");

    // Plot the maillard stage line
    chart.draw_series(LineSeries::new(
        temp.iter().map(|(x, _)| (*x, 160.0_f32)),
        &GRAY,
    ))?;
    debug!("plotted 1st crack");

    // Plot the first crack temp line.
    chart.draw_series(LineSeries::new(
        temp.iter().map(|(x, _)| (*x, 196.0_f32)),
        &GRAY,
    ))?;
    debug!("plotted 1st crack");

    // Plot the second crack temp line.
    chart.draw_series(LineSeries::new(
        temp.iter().map(|(x, _)| (*x, 224.0_f32)),
        &GRAY,
    ))?;
    debug!("plotted 2nd crack");

    // Label the series.
    chart
        .configure_series_labels()
        //.background_style(&TRANSPARENT)
        //.border_style(&BLACK)
        .draw()?;
    debug!("configured series labels");

    // Drop root and chart so we can return path.
    drop(chart);
    drop(root);
    debug!("dropped chart and root");

    // Return the path so the caller knows where to find the image.
    Ok(PlotPath(path))
}

#[cfg(test)]
mod tests {
    extern crate test;

    use super::*;
    use rand::{thread_rng, Rng};
    use test::{black_box, Bencher};

    fn readings() -> Vec<(u64, f32)> {
        let mut rng = thread_rng();
        let mut readings = vec![];
        for x in 0..600 {
            let diff = rng.gen_range(-3.0, 3.0);
            readings.push((x, readings.last().unwrap_or(&(0, 0.0)).1 + diff));
        }

        readings
    }

    #[test]
    fn test_plot() {
        let readings = readings();

        for path in &[
            plot_series(320, 175, &readings, &readings).unwrap(),
            plot_series(320, 175, &[], &[]).unwrap(),
            plot_series(320, 175, &[(2, 23.0)], &[]).unwrap(),
            plot_series(320, 175, &[], &[(62, 0.0), (64, 0.0)]).unwrap(),
        ] {
            // TODO: Use display macro here
        }
    }

    #[bench]
    fn bench_plot(b: &mut Bencher) {
        let readings = readings();

        b.iter(|| plot_series(320, 175, black_box(&readings), black_box(&readings)).unwrap());
    }

    #[bench]
    fn bench_domain_and_range(b: &mut Bencher) {
        let readings = readings();

        b.iter(|| domain_and_range(black_box(&readings)));
    }
}
