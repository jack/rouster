/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::{bail, Result};
use crossbeam::channel;
use log::{debug, error, info, trace};
use rand::{thread_rng, Rng};
use spidev::{SpiModeFlags, Spidev, SpidevOptions};
use std::io::Read;

use super::Event;

pub(crate) async fn fake(tx: channel::Sender<Event>) -> Result<()> {
    let mut val = 21.0;
    let mut rng = thread_rng();

    loop {
        let diff = rng.gen_range(-3.0, 3.0);
        val += diff;
        if tx.send(Event::Temperature(val)).is_err() {
            error!("error sending temperature because the channel is disconnected");
            bail!("error sending temperature because the channel is disconnected");
        }
    }
}

pub(crate) async fn spi(spi_device: String, snd: channel::Sender<Event>) -> Result<()> {
    info!("Attempting to set up thermocouple on {}", &spi_device);
    let mut spi = create_spi(&spi_device).unwrap();

    loop {
        // Read the temperature
        let r = half_duplex(&mut spi).unwrap();
        let (ext, int) = parse_temp(r);
        trace!(
            "temperature: ext: {:?} ({:?} f), int: {:?}",
            ext,
            fahrenheit(ext).round(),
            int
        );

        // Send the temperature reading to the main event loop.
        if snd.send(Event::Temperature(ext)).is_err() {
            bail!("error sending temperature because the channel is disconnected");
        }

        debug!("sent temperature through channel");
    }
}

// Create an spidevice?
fn create_spi(path: &str) -> Result<Spidev> {
    let mut spi = Spidev::open(path)?;
    let options = SpidevOptions::new()
        .bits_per_word(8)
        .max_speed_hz(20_000)
        .mode(SpiModeFlags::SPI_MODE_0)
        .build();
    spi.configure(&options)?;

    Ok(spi)
}

// Perform half duplex operations using Read and Write traits
fn half_duplex(spi: &mut Spidev) -> Result<u32> {
    let mut rx_buf = [0_u8; 4];
    let byte_count = spi.read(&mut rx_buf)?;
    if byte_count != 4 {
        error!("did not receive 4 8-bit bytes from the SPI temperature read");
    }

    // bit-shift the 4 8-bit bytes we got over into a u32 32bit byte
    let big: u32 = u32::from(rx_buf[0]) << 24
        | u32::from(rx_buf[1]) << 16
        | u32::from(rx_buf[2]) << 8
        | u32::from(rx_buf[3]);
    trace!("spi read result rx_buf: {:032b}", big);

    Ok(big)
}

fn fahrenheit(c: f32) -> f32 {
    c * 1.8 + 32.0
}

fn parse_temp(raw: u32) -> (f32, f32) {
    // External temperature.
    let whole_mask = 0b0_1111111111100_00_000000000000_0000_u32;
    let frac_mask = 0b0_0000000000011_00_000000000000_0000_u32;
    let external = if raw & (1 << 31) == 0 {
        let whole = (raw & whole_mask) >> 20;
        let frac = (raw & frac_mask) >> 18;
        whole as f32 + frac as f32 * 0.25
    } else {
        let whole = (!raw & whole_mask) >> 20;
        let frac = (raw & frac_mask) >> 18;
        eprintln!("{:?}, {:?}", whole, frac);
        -1.0 - whole as f32 + frac as f32 * 0.25
    };

    // Internal temperature.
    let whole_mask = 0b0_0000000000000_00_011111111100_0000_u32;
    let frac_mask = 0b0_0000000000000_00_000000001111_0000_u32;

    let internal = if raw & (1 << 15) == 0 {
        let whole = (raw & whole_mask) >> 8;
        let frac = (raw & frac_mask) >> 4;
        whole as f32 + frac as f32 * 0.0625
    } else {
        let whole = (!raw & whole_mask) >> 8;
        let frac = (raw & frac_mask) >> 4;
        -1.0 - whole as f32 + frac as f32 * 0.0625
    };

    (external, internal)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_temp() {
        // Test some positive whole numbers
        assert_eq!(
            parse_temp(0b01100100000000000111111100000000_u32),
            (1600.0, 127.0)
        );
        assert_eq!(
            parse_temp(0b00111110100000000110010010010000_u32),
            (1000.0, 100.5625)
        );

        // Test some negative whole numbers
        assert_eq!(
            parse_temp(0b1111_1111_1111_00_001110110000000000_u32),
            (-1.0, -20.0)
        );
        assert_eq!(
            parse_temp(0b11110000011000001100100100000000_u32),
            (-250.0, -55.0)
        );

        // Test some positive fractional numbers
        assert_eq!(
            parse_temp(0b00000110010011000001100100000000_u32),
            (100.75, 25.0)
        );

        // Test some negative fractional numbers
        assert_eq!(
            parse_temp(0b1111_1111_1111_11_00_1111_1111_0000_0000_u32),
            (-0.25, -1.0)
        );
    }
}
