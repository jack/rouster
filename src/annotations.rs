/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::Result;
use log::info;
use reqwest::Client;
use serde::Serialize;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
struct Annotation {
    time: u128,
    time_end: u128,
    tags: Vec<String>,
    text: String,
}

pub(crate) async fn grafana(token: String, msg: &'static str) -> Result<()> {
    let req = Client::new()
        .post("http://grafana:3000/api/annotations")
        .bearer_auth(token)
        .json(&Annotation {
            time: SystemTime::now().duration_since(UNIX_EPOCH)?.as_millis(),
            time_end: SystemTime::now().duration_since(UNIX_EPOCH)?.as_millis(),
            text: String::from(msg),
            tags: vec![String::from("rouster")],
        });
    info!("annotation request: {:?}", &req);

    let res = req.send().await?;
    info!("annotation response: {:?}", &res);

    Ok(())
}
