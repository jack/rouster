/*
Rouster is software to monitor and control home coffee roasters.
Copyright (C) 2020  Jack Brown

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#![feature(test)]
#![feature(thread_id_value)]

use crate::framebuffer::{FakeFramebuffer, Framebuffer, RealFramebuffer};
use anyhow::Result;
use canvas::{Alignment, Canvas};
use chrono::offset::Local;
use crossbeam::channel;
use lazy_static::lazy_static;
use log::{debug, info, trace, warn};
use prometheus::{Gauge, IntCounter, Registry};
use std::{
    env,
    path::PathBuf,
    thread::sleep,
    time::{Duration, Instant},
};
use structopt::StructOpt;
use tokio;

mod annotations;
mod canvas;
mod error;
mod events;
mod framebuffer;
mod gpio;
mod notification;
pub mod plot;
mod storage;
mod temperature;
mod web;

use events::Event;

#[macro_use]
mod macros;

pub extern crate std;

const GRAY: &str = "#4c4c4c";

lazy_static! {
    static ref REGISTRY: Registry = Registry::new();
    static ref TEMP_DATAPOINTS: IntCounter = IntCounter::new(
        "rouster_temperature_datapoints",
        "number of collected temperature datapoints"
    )
    .unwrap();
    static ref RENDERS: IntCounter =
        IntCounter::new("rouster_renders", "number of frames rendered").unwrap();
    static ref TEMPERATURE_BEANS: Gauge =
        Gauge::new("rouster_temperature_beans", "temperature in the beans").unwrap();
}

#[derive(Debug, StructOpt)]
#[structopt(name = "rouster", about = "Display roast data!")]
struct Opt {
    /// Set how many times to run through the loop. Not passing this will cause
    /// the process to run indefinitely.
    #[structopt(short = "l", long = "loops")]
    loops: Option<u64>,
}

#[tokio::main]
async fn main() -> Result<()> {
    REGISTRY
        .register(Box::new(TEMP_DATAPOINTS.clone()))
        .unwrap();
    REGISTRY
        .register(Box::new(TEMPERATURE_BEANS.clone()))
        .unwrap();
    REGISTRY.register(Box::new(RENDERS.clone())).unwrap();

    env_logger::builder().format_timestamp_millis().init();

    let opt = Opt::from_args();
    debug!("{:?}", opt);

    run(opt.loops).await.unwrap();

    Ok(())
}

/// Run the main event loop. Pass loops to run a finite number of times. Mostly
/// useful for testing/benchmarking.
async fn run(loops: Option<u64>) -> Result<u64> {
    // Record the startup time. This is used for the timer, and is reset when
    // the reset button is pressed.
    let mut start = Instant::now();

    // Figure out where to store images on the local disk.
    let image_path = if let Ok(p) = env::var(&"IMAGE_PATH") {
        PathBuf::from(p)
    } else {
        PathBuf::from("/tmp")
    };

    // Figure out which framebuffer to use; fall back to the built-in fb0 so we
    // can run headless without crashing.
    let mut fb: Box<dyn Framebuffer> = if let Ok(device) = env::var(&"FRAMEBUFFER") {
        Box::new(RealFramebuffer::new(device).unwrap())
    } else {
        warn!("missing or bad FRAMEBUFFER env var; using dummy framebuffer",);
        Box::new(FakeFramebuffer::new("/tmp/rousterbuffer", 640, 480))
    };
    debug!("{:?}", fb);

    // Figure out which storage backend to use; fall back to dummy storage which
    // does nothing.
    let storage: Box<dyn storage::Storage> = if let Ok(key_id) = env::var(&"B2_KEY_ID") {
        info!("configuring backblaze b2 for graph uploads");
        let key = env::var(&"B2_KEY")?;
        let bucket_id = env::var(&"B2_BUCKET_ID")?;
        Box::new(storage::b2::Client::new(&key_id, &key, &bucket_id))
    } else {
        warn!("No B2 storage configured; not uploading");
        Box::new(storage::dummy::Client {})
    };

    // Figure out which notification backend to use; fall back to dummy storage
    // which does nothing.
    let notification: Box<dyn notification::Notification> =
        if let Ok(account_sid) = env::var(&"TWILIO_ACCOUNT_SID") {
            info!("configuring twilio for sending profile images");
            let auth_token = env::var(&"TWILIO_AUTH_TOKEN")?;
            let from = env::var(&"TWILIO_FROM")?;
            Box::new(notification::twilio::Client::new(
                &account_sid,
                &auth_token,
                &from,
            ))
        } else {
            warn!("No notification providers configured; not sending profiles");
            Box::new(notification::dummy::Client {})
        };

    // Get all the phone numbers we need to send notifications to.
    let phone_numbers: Vec<String> = if let Ok(numbers) = env::var(&"PHONE_NUMBERS") {
        info!("sending notifications to {:?}", numbers);
        numbers.split(",").map(|s| s.to_string()).collect()
    } else {
        warn!("no phone numbers configured for profile notifications");
        vec![]
    };

    // Create a channel for communicating events.
    let (tx, messages) = channel::bounded(1);
    // Spawn a new thread to wait for a reset button press, but only if there's
    // a reset pin specificied (and connected, presumably). With no reset pin,
    // the roaster can be reset via the API.
    if let Ok(pin) = env::var(&"RESET_GPIO_PIN") {
        let pin = pin.parse().unwrap();

        info!("listening for reset events on pin {}", pin);
        gpio::interrupt(pin, Event::NoOp, Event::Reset, tx.clone());
    }

    // Spawn a new thread to wait for a reset button press, but only if there's
    // a reset pin specificied (and connected, presumably). With no runhold pin,
    // the roaster can enter run/hold states via the API.
    if let Ok(pin) = env::var(&"HOLD_RUN_GPIO_PIN") {
        let pin = pin.parse().unwrap();

        info!("listening for hold/run events on pin {}", pin);
        gpio::interrupt(pin, Event::NoOp, Event::RunHold, tx.clone());
    }

    // If we set an SPI device (like /dev/spi1.0) then we'll set up temp
    // readings. Otherwise the device will just always read 0. On the pi, by
    // default SPI /dev/spidev1.0 has its digital input pin on BCM GPIO18, Pi
    // pinout pin 12.
    match env::var(&"SPI_DEVICE") {
        Ok(spi_path) if spi_path == "fake" => {
            tokio::spawn(temperature::fake(tx.clone()));
        }
        Ok(spi_path) => {
            tokio::spawn(temperature::spi(spi_path, tx.clone()));
        }
        Err(_) => {
            warn!("no SPI_DEVICE set; no temperature will be displayed");
        }
    }

    // Snooze can optionally be set to slow down the running of the program.
    // This is useful for debugging.
    let snooze = env::var("LOOP_DELAY_MS")
        .map(|s| s.parse::<u64>().unwrap())
        .map(Duration::from_millis)
        .ok();

    // Initialize the last frame time to now. This will make the first frame
    // delay calculation wrong, but it won't matter after that.
    let mut last_frame = Instant::now();

    // readings is where we'll collect temperature readings.
    let mut readings = vec![];
    // rors is where we'll collect rate of rise calculations once we have enough readings.
    let mut rors = vec![];
    // derivatives is experimental, and is where we'll put reading over reading
    // deltas to see if they provide better real time feedback of temperature control.
    let mut derivatives = vec![];

    // Whether or not to pause execution
    let mut hold = false;
    // When the hold was begun. Since we use relative time (monotonically
    // increasing time since reset), after a hold event we need to advance the
    // start variable to by the length of the hold.
    let mut hold_start = Instant::now();

    // Start a web server asynchronously, so we can continue with our loop while
    // serving prometheus metrics and our api.
    tokio::task::spawn(web::serve_metrics(tx));

    // Start our event loop, which will just run forever
    let mut count = 1;
    loop {
        debug!("\nstarting loop iteration");

        // Make sure we haven't hit our loop max, if one was passed.
        if let Some(max) = loops {
            if count > max {
                break;
            }
        }

        // Check for events coming in from temp readings or button presses.
        if let Ok(msg) = messages.try_recv() {
            match msg {
                Event::RunHold => {
                    info!("run/hold event received");

                    // Toggle hold true/false. This means every time we get a
                    // run/hold event, we'll resume if currently on hold, or hold if
                    // currently running.
                    hold = !hold;

                    // If hold is true, we're entering a hold. If it's false,
                    // we're coming out of a hold.
                    if hold {
                        info!("entering hold");
                        // If we're starting a hold, note the time.
                        hold_start = Instant::now();

                        async {
                            // Send an annotation to Grafana, but only if the token
                            // is set.
                            if let Ok(token) = env::var("GRAFANA_TOKEN") {
                                tokio::spawn(annotations::grafana(token, "Entering hold"));
                            } else {
                                warn!("missing GRAFANA_TOKEN; not setting annotation");
                            }

                            // Save the image to disk
                            let canvas = render(
                                1920,
                                1080,
                                &start.elapsed(),
                                &last_frame.elapsed(),
                                &readings,
                                &rors,
                            )
                            .unwrap();
                            let path = &image_path.join(
                                Local::now()
                                    .format(r#"rouster-%Y-%m-%d-%H:%M-%z.png"#)
                                    .to_string(),
                            );
                            canvas.write(&path).unwrap();
                            info!("wrote image to {:?}", &path);

                            // Upload the image to configured storage.
                            let url = storage.upload(&path)?;

                            // Notify any users that it's been uploaded.
                            for number in &phone_numbers {
                                notification.send(number, Some(&url))?;
                            }

                            Ok::<(), anyhow::Error>(())
                        }
                        .await?
                    } else {
                        info!("leaving hold; running");
                        // If we're ending a hold, advance the "start" time by
                        // the amount of time we held, to preserve our virtual
                        // elapsed time. This works, since we don't care about
                        // absolute time for the start value.
                        start = start.checked_add(hold_start.elapsed()).unwrap();
                    }
                }
                Event::NoOp => {}
                Event::Reset => {
                    // If the button has been pressed, reset the timer. This will both
                    // register that the button was pressed, and reset the value of reset
                    // back to 1, ready for another reset on the next time through the loop.
                    info!("reset button press received; resetting timer");
                    start = Instant::now();

                    info!("reset button press received; resetting temperature readings");
                    readings.clear();
                    rors.clear();
                    TEMP_DATAPOINTS.reset();

                    info!("reset button press received; setting to run");
                    hold = false;
                    hold_start = start;
                }
                Event::Temperature(temp) => {
                    if hold {
                        continue;
                    }
                    // Increment the prometheus counter for this metric.
                    TEMP_DATAPOINTS.inc();
                    TEMPERATURE_BEANS.set(temp as f64);

                    let ts = start.elapsed().as_secs();

                    // If we've already got a reading, we can calculate the rate of
                    // change between readings.
                    if let Some((prev_ts, prev_temp)) = readings.last() {
                        // Use basic math to calculate rise over run
                        let delta = (temp - prev_temp) / ts.checked_sub(*prev_ts).unwrap() as f32;
                        derivatives.push((ts, delta));
                    }

                    readings.push((ts, temp));
                    if let Some(ror) = ror(&readings) {
                        rors.push((ts, ror));
                    }
                }
            }
        }

        // If we're held, pause execution.
        if hold {
            sleep(Duration::from_millis(500));

            continue;
        }

        debug!("elapsed: {}s", &start.elapsed().as_secs());
        let canvas = render(
            fb.width() as u64,
            fb.height() as u64,
            &start.elapsed(),
            &last_frame.elapsed(),
            &readings,
            &rors,
        )
        .unwrap();
        debug!("done rendering");
        RENDERS.inc();

        // Reset the frame timer for the next frame.
        last_frame = Instant::now();

        // Build the frame to send to the buffer
        let frame = canvas.frame().unwrap();
        debug!("done building frame");

        // Write the completed image to the buffer
        fb.write(&frame);
        debug!("done writing frame to fb");

        debug!("main loop iteration complete");

        // Increment the loop counter.
        count += 1;

        // Sleep if we've set up a delay
        if let Some(snooze) = snooze {
            sleep(snooze);
        }
    }

    Ok(count - 1)
}

fn render(
    width: u64,
    height: u64,
    start: &Duration,
    last_frame: &Duration,
    readings: &[(u64, f32)],
    rors: &[(u64, f32)],
) -> Result<Canvas> {
    // Create a canvas that we'll use to build the image to display.
    let mut canvas = Canvas::new(width, height, Some("white")).unwrap();

    // Add the current timestamp.
    let now = Local::now().format("%Y-%m-%d %H:%M %Z").to_string();
    canvas
        .annotate(now, 5.0, 0.0, 0.0, &Alignment::TopLeft, &"black")
        .unwrap();

    // Add some vanity credit :-)
    canvas
        .annotate("Rouster", 4.0, 0.0, 0.0, &Alignment::BottomRight, GRAY)
        .unwrap();

    // Track the amount of time elapsed between renders.
    let fps = format!("{:.2} fps", 1.0 / last_frame.as_secs_f32());
    canvas
        .annotate(fps, 4.0, 0.0, 0.0, &Alignment::BottomLeft, GRAY)
        .unwrap();

    // Add the elapsed time since the last reset.
    canvas
        .annotate("elapsed m:s", 5.0, 0.0, 6.0, &Alignment::TopLeft, GRAY)
        .unwrap();
    let elapsed = format!("{:2}:{:02}", &start.as_secs() / 60, &start.as_secs() % 60);
    canvas
        .annotate(elapsed, 12.0, 0.0, 10.0, &Alignment::TopLeft, &"black")
        .unwrap();

    // Add the temp labels
    canvas
        .annotate("temp \u{b0}C", 5.0, 0.0, 6.0, &Alignment::Top, GRAY)
        .unwrap();
    canvas
        .annotate("RoR \u{b0}C", 5.0, 0.0, 6.0, &Alignment::TopRight, GRAY)
        .unwrap();

    // If we have any readings, render them.
    if let Some((_, temp)) = readings.last() {
        trace!("current temperature: {:?}", temp);

        // Render the current temp. \u{b0} is unicode for the degree symbol.
        let current_temp = format!("{:3.1}\u{b0}", temp);
        canvas
            .annotate(current_temp, 12.0, 0.0, 10.0, &Alignment::Top, &"red")
            .unwrap();

        // Render the RoR (Rate of Rise).
        let current_ror = format!(
            "{}\u{b0}",
            ror(readings).map_or("?".to_string(), |r| format!("{:2.1}", &r))
        );
        canvas
            .annotate(current_ror, 12.0, 0.0, 10.0, &Alignment::TopRight, "blue")
            .unwrap();

        // Render the max temp
        let max_temp = readings
            .iter()
            .max_by(|a, b| a.1.partial_cmp(&b.1).unwrap())
            .unwrap();
        let max_temp = format!(
            "max {:.1}\u{b0}@{}",
            max_temp.1,
            min_secs(&Duration::from_secs(max_temp.0))
        );
        canvas
            .annotate(max_temp, 5.0, 0.0, 0.0, &Alignment::TopRight, &"red")
            .unwrap();

        debug!("about to plot series");
        // Add a plot
        canvas.plot_series(readings, rors).unwrap();
        debug!("finished plotting series");
    } else {
        // Print out that we have no data.
        canvas
            .annotate(
                "NO TEMP. DATA",
                12.0,
                0.0,
                0.0,
                &Alignment::Center,
                &"black",
            )
            .unwrap();
    }

    Ok(canvas)
}

/// Format a duration as min:secs, zero-padded.
fn min_secs(raw: &Duration) -> String {
    format!("{}:{:02}", raw.as_secs() / 60, raw.as_secs() % 60)
}

fn ror(readings: &[(u64, f32)]) -> Option<f32> {
    if readings.len() < 2 {
        return None;
    }

    // Get the most recent elapsed duration in seconds.
    let now = readings.last().unwrap().0;

    // If we don't have at least 60 seconds of data, return None.
    if now - readings.first().unwrap().0 < 60 {
        return None;
    }

    // Initialize our current and previous reading to the
    // current value; if we don't have 30s worth of data,
    // this will ensure we essentially get a 0.
    let mut current_reading = readings.last().unwrap().1;
    let mut previous_reading = f32::NAN;

    // Go through and collect average temps for the immediately preceeding 30
    // seconds, and seconds 30-60 seconds ago. This should give some smoothing
    // so we don't jump around all the time. Plus, we're sampling multiple times
    // per second, in theory, so this will ensure we don't pick a randomly
    // anomalous value for the time.
    //
    // We'll start at the back of the slice and work backwards. Not sure if
    // there's a better way to do this in rust.
    let mut i = readings.len();
    while i > 0 {
        trace!(
            "==> current_reading: {:?}, previous_reading: {:?}",
            current_reading,
            previous_reading
        );
        // Pull out the timestamp and reading for the item.
        let (timestamp, temperature) = readings[i - 1];
        trace!("timestamp: {:?}, temperature: {:?}", timestamp, temperature);

        // If we're in the first 30 seconds, add it to the current_reading
        // average.
        if timestamp > now - 30 {
            trace!("found current timestamp");
            current_reading = (current_reading + temperature) / 2.0;
        }

        // If we're between 30 and 60 seconds ago, add it to the
        // previous_reading average.
        if timestamp <= now - 30 && timestamp > now - 60 {
            trace!("found previous timestamp");
            if previous_reading.is_nan() {
                previous_reading = temperature;
                continue;
            }

            previous_reading = (previous_reading + temperature) / 2.0;
        }

        // If we've gone back further than 35 seconds ago, we're done.
        if timestamp < now - 60 {
            break;
        }

        // Decrement the position in the vec.
        i -= 1;
    }

    trace!(
        "done with while loop; current_reading: {:?}, previous_reading: {:?}",
        current_reading,
        previous_reading
    );

    // If we didn't have any values from 30-60 seconds ago,
    // just return the average temp over the last 30 seconds.
    if previous_reading.is_nan() {
        return Some(current_reading);
    }

    let ror = current_reading - previous_reading;

    Some(ror)
}

#[cfg(test)]
mod tests {
    extern crate test;

    use super::*;
    use rand::{thread_rng, Rng};
    use test::{black_box, Bencher};

    const WIDTH: u64 = 320;
    const HEIGHT: u64 = 240;

    /// readings returns a tuple of (temperatures, rors) for test cases.
    fn readings() -> (Vec<(u64, f32)>, Vec<(u64, f32)>) {
        let mut rng = thread_rng();
        let mut readings = vec![];
        for x in 0..600 {
            let diff = rng.gen_range(-3.0, 3.0);
            readings.push((x, readings.last().unwrap_or(&(0, 0.0)).1 + diff));
        }

        let mut rors = vec![];
        for (i, (x, _y)) in readings.iter().enumerate() {
            if let Some(ror) = ror(&readings[0..=i]) {
                rors.push((*x + 600, ror));
            }
        }

        (readings, rors)
    }

    #[test]
    fn test_run() {
        // Make sure that if we pass 0, the loop never runs.
        //assert_eq!(run(Some(0)).unwrap(), 0);

        // Make sure that if we run once through the loop, it only runs once.
        //assert_eq!(run(Some(1)).unwrap(), 1);

        // Make sure that if we run 10 times through the loop, it runs exactly
        // 10 times.
        //assert_eq!(run(Some(1)).unwrap(), 1);
    }

    // This benchmarks at 185ms as of 2020-06-11, commit 929b6e5e.
    #[bench]
    fn bench_run(b: &mut Bencher) {
        b.iter(|| run(Some(1)));
    }

    #[test]
    fn test_render() {
        let t = Duration::new(68, 0);

        let (readings, rors) = readings();

        let canvas = render(WIDTH, HEIGHT, &t, &t, &readings, &rors).unwrap();
        display_image!(&canvas, "integration");
        let canvas = render(WIDTH * 3, HEIGHT * 3, &t, &t, &readings, &rors).unwrap();
        display_image!(&canvas, "integration");
        let canvas = render(1920, 1080, &t, &t, &readings, &rors).unwrap();
        display_image!(&canvas, "integration");
    }

    #[bench]
    // This benchmarks at 185ms as of 2020-06-11, commit 929b6e5e.
    fn bench_render(b: &mut Bencher) {
        let t = Duration::new(68, 0);
        let (readings, rors) = readings();

        b.iter(|| {
            render(
                black_box(WIDTH),
                black_box(HEIGHT),
                black_box(&t),
                black_box(&t),
                black_box(&readings),
                black_box(&rors),
            )
        });
    }

    #[test]
    #[test]
    fn test_ror() {
        // Empty should return None.
        assert!(matches!(ror(&vec![]), None));

        // With no timestamps from the 30-60s range, it should return the last 30s
        // avg.
        assert_eq!(ror(&vec![(0, 0.0), (60, 1.0)]).unwrap(), 1.0);

        // With a timestamp in the past, it should be the difference, positive if
        // the newer value is higher.
        assert_eq!(ror(&vec![(0, 20.0), (60, 0.0), (100, 1.0)]).unwrap(), 1.0);

        // With a timestamp in the past, it should be the difference, negative if
        // the newer value is lower.
        assert_eq!(ror(&vec![(0, 2.0), (60, 15.0), (100, 1.0)]).unwrap(), -14.0);
    }
}
