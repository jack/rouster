# Rouster

## Architecture

The hardware for the system consists of a pi, display, thermocouple (over SPI),
and a double-throw momentary toggle switch (essentially 2 switches).

When the process starts, it reads env vars to determine where the framebuffer,
thermocouple and switches are connected. If there is no `FRAMEBUFFER` set, it uses
a `FakeFramebuffer` backend to allow running the program "headless".

* temperature
* reset
* run/hold

If the SPI_DEVICE for the thermocouple is set to `fake`, it instead spawns a
thread that generates fake, randomized temperature data, so you can test
everything without a thermocouple.

Rouster also runs a webserver on port 8080 by default that allows controlling
things via HTTP. Check out web.rs to see what all is available. TODO: Flesh
out the docs here.

``` sh
echo 0 | socat - UNIX-CONNECT:/tmp/rouster-reset-<pid>-<threadId>.sock
```

The main event loop then starts, and for every iteration it goes through does
the following:

* Check to see if max loop count has been exceeded (this is only used for
  testing/benchmarks).
* Try to receive from the message channel. If there's a message available,
  handle it. This means collecting temperature data, resetting, entering a hold
  state, or resuming from hold to run.
* Check to see if `hold` is enabled. If it is, sleep for 500ms and continue the
  loop at the beginning of the next iteration. Don't count this as an executed
  loop for the max loop counter.
* Render an image to a buffer in memory, compositing all the data/plots, etc.
* Convert the in-memory image to a `Vec<u8>` that the framebuffer can digest.
* Write the vector to the framebuffer.
* Increment the loop counter.
* If snooze is set, delay by the amount specified by snooze.

## Testing

Run `make test`. You can filter tests by setting `t`: `make test t=test_render`.
To view the composited image, you need to enable the `feh` feature (and have
`feh` installed on your system), or just use `make integration`. Test coverage
is not great. There's a LOT of room for improvement around better tests, and
re-architecting some functionality to allow more robust testing.

## Linting

I strongly suggest installing `clippy` (`rustup component add clippy`) and
running `cargo clippy` when making changes. In pedantic mode especially, clippy
is very informative.

## Benchmarking

Run `make bench`. Benchmark coverage is pretty incomplete.

## Known Issues / Flaws

* The main event loop is pretty overloaded, code-wise. The function is too long,
  and should make more function calls out to functional components. This is a
  TBD project, and needs a bit of thought to make it helpfully split up.
* All messages go through a single event channel.
* The graphics are made by compositing an in-memory imagemagick image with a PNG
  on disk that's created by the `plotters` library. `plotters` has some support
  for in-memory operations, but it wasn't quite clear how to get something out
  of it that imagemagick could process. In theory, that's one of the slowest
  parts right now.
* There's no support for long-press button functionality. It would be nice to
  overload button functionality by supporting long and short presses.
* There's some excessive abstraction in place. I've wrapped the `framebuffer`
  and `plotters` crates to provide a cleaner interface for this application, but
  then I wound up creating a `canvas.rs` module, which sort of wraps `plotters`
  again. It needs some untangling. I'd like to create more of a builder paradigm
  around constructing the composite image, but need to get the plotters library
  better integrated.
* The rate of rise is plotted on secondary axes, but I can't figure out how to
  re-use the x axis and just get a second y axis. This looks fine, but is subtly
  wrong, because it's actually stretched out along the whole x axis, instead of
  lining up with the same timeline that the primary y-axis temp graph is showing.
* Many more...

