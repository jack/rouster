.DEFAULT_GOAL := build
SHELL := /bin/bash

machine := $(shell uname -m | sed 's/x86_64/amd64/')

# Which tests to run
t ?=

.PHONY: build
build: target/doc
	cargo build

.PHONY: test
test:
	cargo test --no-fail-fast $(t) -- --nocapture --test-threads=1

.PHONY: integration
integration:
	cargo test --features feh --no-fail-fast $(t) -- --nocapture --test-threads=1

.PHONY: bench
bench:
	cargo bench

Dockerfile: Dockerfile.template
	sed 's/%%BALENA_MACHINE_NAME%%/intel-nuc/' Dockerfile.template > Dockerfile

Dockerfile.local.prometheus: Dockerfile.prometheus
	sed 's|FROM prom/prometheus.*|FROM prom/prometheus:v2.20.1|' \
		Dockerfile.prometheus > Dockerfile.local.prometheus

Dockerfile.local.haproxy: Dockerfile.haproxy
	sed 's|FROM library/haproxy.*|FROM library/haproxy:2.2.4|' \
		Dockerfile.haproxy > Dockerfile.local.haproxy

docker-compose.env: .env
	echo "# GENERATED FILE: DO NOT EDIT!!!!" > docker-compose.env
	sed 's/export //' .env >> docker-compose.env

# This will only work when run on an x86_64 device.
.PHONY: image
image: Dockerfile
	docker build -t localhost/rouster:latest .

.PHONY: dockerfiles
dockerfiles: Dockerfile Dockerfile.local.prometheus Dockerfile.local.haproxy

# Pass both docker-compose.yml and docker-compose.local.yml to apply the
# overrides from the local compose file.
.PHONY: up
up: dockerfiles docker-compose.yml docker-compose.$(machine).yml docker-compose.env
	docker-compose -f docker-compose.yml -f docker-compose.$(machine).yml up --build

# This will only work when run on an ARM device compatible with the rpi3.
.PHONY: pi
pi: Dockerfile.template
	sed 's/%%BALENA_MACHINE_NAME%%/raspberrypi3/' Dockerfile.template > Dockerfile
	docker build -t localhost/rouster:latest .

.PHONY: lint
lint: src/*.rs src/**/*.rs
	cargo clippy -- -W clippy::pedantic

# Run the code locally. This will only work if you're on a pi or have some
# device with an available framebuffer.
.PHONY: run
run:
	. .env \
	&& RUST_BACKTRACE=1 RUST_LOG=info LOOP_DELAY_MS=500 SPI_DEVICE=fake cargo run

.PHONY: perf
perf:
	cargo build --release
	. .env \
	&& RUST_BACKTRACE=full RUST_LOG=info SPI_DEVICE=fake \
		perf record -g target/release/rouster --loops 100
	perf report

.PHONY: release
release: test
	cargo test --release --no-fail-fast $(t) -- --nocapture
	cargo build --release

target/doc: Cargo.lock Cargo.toml
	cargo doc

.PHONY: install
install: test
	cargo install --path . --force

.PHONY: clean
clean:
	rm -rf \
		target \
		perf.data*

