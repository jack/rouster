################################################################################
# Base image
################################################################################

FROM balenalib/%%BALENA_MACHINE_NAME%%-debian as base

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -q update \
  && apt-get install -yq --no-install-recommends \
    build-essential \
    curl \
    libfontconfig1-dev \
    libfreetype6-dev \
    libmagickwand-dev \
    libpng-dev \
    libssl-dev \
    pkg-config \
    socat

# Why do we need to install our own version of imagemagick?
ENV MAGICK_VERSION=7.0
RUN curl https://www.imagemagick.org/download/ImageMagick.tar.gz | tar xz \
 && cd ImageMagick-${MAGICK_VERSION}* \
 && ./configure --with-magick-plus-plus=no --with-perl=no \
 && make \
 && make install \
 && cd .. \
 && rm -r ImageMagick-${MAGICK_VERSION}*

################################################################################
# Rust image
################################################################################

FROM base as rust

# Install build tools
RUN apt-get -q update \
  && apt-get install -yq --no-install-recommends \
    clang \
    cmake \
    file \
    protobuf-compiler

ENV PATH=/root/.cargo/bin:$PATH

# Install Rust
RUN curl \
        --proto '=https' \
        --tlsv1.2 -sSf https://sh.rustup.rs \
    | sh -s -- --default-toolchain nightly-2020-10-13 -y

################################################################################
# Dependencies
################################################################################

FROM rust as dependencies

WORKDIR /build

# Create new fake project ($USER is needed by `cargo new`)
RUN USER=root cargo new app

WORKDIR /build/app

# Copy real app dependencies
COPY Cargo.* ./

# Build fake project with real dependencies
RUN cargo build --release

# Remove the fake app build artifacts
#
# NOTE If your application name contains `-` (`foo-bar` for example)
# then the correct command to remove build artifacts looks like:
#
# RUN rm -rf target/release/foo-bar target/release/deps/foo_bar-*
#                              ^                           ^
RUN rm -rf target/release/rouster* target/release/deps/rouster-*

################################################################################
# Builder
################################################################################

FROM rust as builder

# We do not want to download deps, update registry, ... again
COPY --from=dependencies /root/.cargo /root/.cargo

WORKDIR /build/app

# Copy Cargo files + source
COPY Cargo.toml Cargo.lock ./
COPY src ./src

# Update already built deps from dependencies image
COPY --from=dependencies /build/app/target target

# Build real app
RUN cargo build --release
# For some reason, the release tests fail on arm
#RUN cargo test --release -- --test-threads=1

################################################################################
# Final image
################################################################################

FROM base

EXPOSE 8080/tcp
WORKDIR /app

# Copy binary from builder image
COPY --from=builder /build/app/target/release/rouster .
COPY assets ./assets

ENV LD_LIBRARY_PATH=/usr/local/lib
RUN ldconfig

# Launch application
CMD ["./rouster"]
